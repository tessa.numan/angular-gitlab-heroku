import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Musical details wijzigen',
      description: 'Hiermee kunnen de details van een musical worden gewijzigd.',
      scenario: [
        'Gebruiker navigeert naar de lijstpagina met musicals.',
        'De applicatie toont de ingevoerde musicals.',
        'Gebruiker klikt een musical aan.',
        'De applicatie toont de details van de aangeklikte musical.',
        'Gebruiker drukt op de edit knop.',
        'De applicatie toont een formulier waar de musical kan worden aangepast.',
        'Gebruiker past de gegevens aan en drukt op opslaan.',
        'De applicatie toont de gewijzigde musical.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'De actor is ingelogd.',
      postcondition: 'Musical is aangepast en wordt getoond.'
    },
    {
      id: 'UC-03',
      name: 'Musical verwijderen',
      description: 'Hiermee kan een musical worden verwijderd.',
      scenario: [
        'Gebruiker navigeert naar de lijstpagina met musicals.',
        'De applicatie toont de ingevoerde musicals.',
        'Gebruiker klikt een musical aan.',
        'De applicatie toont de details van de aangeklikte musical.',
        'Gebruiker drukt op de verwijder knop.',
        'De applicatie verwijderd de musical en toont de lijst met musicals.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'De actor is ingelogd.',
      postcondition: 'De musical is verwijderd.'
    },
    {
      id: 'UC-04',
      name: 'Musical toevoegen',
      description: 'Hiermee kan een musical worden toegevoegd.',
      scenario: [
        'Gebruiker navigeert de pagina om een musicals toe te voegen.',
        'De applicatie toont het formulier om een musical toe te voegen.',
        'Gebruiker vult formulier in, selecteert een bestaande locatie en geen medebezoekers.',
        'Gebruiker drukt op opslaan.',
        'De applicatie controleert ingevulde velden en slaat musical op.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'De actor is ingelogd.',
      postcondition: 'Een nieuwe musical is toegevoegd.'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
