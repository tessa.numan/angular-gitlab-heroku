import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { MusicalsComponent } from './core/musicals/musicals.component'
import { MusicalDetailComponent } from './core/musical-detail/musical-detail.component'
import { MessagesComponent } from './core/messages/messages.component'
import { LoginComponent } from './core/login/login.component'
import { AlertService } from 'src/services/alert.service'
import { AlertModule } from './core/alert/alert.module'
import { UserComponent } from './core/user/user.component'
import { LocationsComponent } from './core/locations/locations.component'
import { CovisitersComponent } from './core/covisiters/covisiters.component'
import { VisitsComponent } from './core/visits/visits.component'
import { LocationDetailComponent } from './core/location-detail/location-detail.component'
import { CovisiterDetailComponent } from './core/covisiter-detail/covisiter-detail.component'
import { VisitDetailComponent } from './core/visit-detail/visit-detail.component'
import { UserAddComponent } from './core/user-add/user-add.component'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsecasesComponent,
    UsecaseComponent,
    DashboardComponent,
    MusicalsComponent,
    MusicalDetailComponent,
    MessagesComponent,
    LoginComponent,
    UserComponent,
    LocationsComponent,
    CovisitersComponent,
    VisitsComponent,
    LocationDetailComponent,
    CovisiterDetailComponent,
    VisitDetailComponent,
    UserAddComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    NgbModule,
    AlertModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [AlertService],
  bootstrap: [AppComponent]
})
export class AppModule {}
