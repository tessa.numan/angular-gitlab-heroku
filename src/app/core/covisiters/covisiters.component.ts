import { Component, OnInit } from '@angular/core'
import { Covisiter } from '../../../models/covisiter.model'
import { CovisiterService } from '../../../services/covisiter.service'
import { MessageService } from '../../../services/message.service'
import { AuthService } from 'src/services/auth.service'
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-covisiters',
  templateUrl: './covisiters.component.html',
  styleUrls: ['./covisiters.component.scss']
})
export class CovisitersComponent implements OnInit {
  covisiters: Covisiter[]
  addBool: boolean
  covisiter: Covisiter

  constructor(
    private route: ActivatedRoute,
    private covisiterService: CovisiterService,
    private authService: AuthService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.covisiters = new Array()
    this.covisiter = new Covisiter()
    this.getCovisiters()
  }

  getCovisiters(): void {
    if (!this.loginCheck()) {
      return
    }
    this.covisiterService.getCovisiters().subscribe(covisiters => {
      this.covisiters = covisiters
      console.log(covisiters)
    })
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn()
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      console.log('log in')
      return false
    }
    return true
  }

  addButton() {
    return (this.addBool = true)
  }

  add(): void {
    if (!this.loginCheck()) {
      return
    }
    if (!this.covisiter.name) {
      return
    }
    this.covisiter.name = this.covisiter.name.trim()
    if (this.covisiters !== null) {
      const found = this.covisiters.find(element => {
        return element.name === this.covisiter.name
      })
      console.log(found)
      if (found !== null && found !== undefined) {
        this.messageService.add(`CovisiterComponent: name already in db`)
        return
      }
    }
    this.covisiterService.addCovisiter(this.covisiter).subscribe(covisiter => {
      this.covisiters.push(covisiter)
    })
  }

  delete(covisiter: Covisiter): void {
    if (!this.loginCheck()) {
      return
    }
    this.covisiters = this.covisiters.filter(m => m !== covisiter)
    this.covisiterService.deleteCovisiter(covisiter).subscribe()
    console.log('Log in!')
  }
}
