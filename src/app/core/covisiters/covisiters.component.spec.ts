import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CovisitersComponent } from './covisiters.component'

describe('CovisitersComponent', () => {
  let component: CovisitersComponent
  let fixture: ComponentFixture<CovisitersComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CovisitersComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CovisitersComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  xit('should create', () => {
    expect(component).toBeTruthy()
  })
})
