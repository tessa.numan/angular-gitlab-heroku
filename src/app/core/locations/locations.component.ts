import { Component, OnInit } from '@angular/core'
import { LocationModel } from '../../../models/location.model'
import { LocationService } from '../../../services/location.service'
import { MessageService } from '../../../services/message.service'
import { AuthService } from 'src/services/auth.service'

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {
  locations: LocationModel[]
  addBool: boolean
  locationModel: LocationModel

  constructor(
    private locationService: LocationService,
    private authService: AuthService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.locations = new Array()
    this.locationModel = new LocationModel()
    this.getLocations()
  }

  getLocations(): void {
    this.locationService.getLocations().subscribe(locations => {
      this.locations = locations
      console.log(locations)
    })
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn()
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      console.log('log in')
      return false
    }
    return true
  }

  addButton() {
    return (this.addBool = true)
  }

  add(): void {
    if (!this.loginCheck()) {
      return
    }
    if (!this.locationModel.name) {
      return
    }
    this.locationModel.name = this.locationModel.name.trim()
    if (this.locations !== null) {
      const found = this.locations.find(element => {
        return element.name === name
      })
      console.log(found)
      if (found !== null && found !== undefined) {
        this.messageService.add(`LocationComponent: name already in db`)
        return
      }
    }
    this.locationService.addLocation(this.locationModel).subscribe(locationIn => {
      this.locations.push(locationIn)
    })
  }

  delete(location: LocationModel): void {
    if (!this.loginCheck()) {
      return
    }
    this.locations = this.locations.filter(m => m !== location)
    this.locationService.deleteLocation(location).subscribe()
    console.log('Log in!')
  }
}
