import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { LocationsComponent } from './locations.component'

describe('LocationsComponent', () => {
  let component: LocationsComponent
  let fixture: ComponentFixture<LocationsComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LocationsComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  xit('should create', () => {
    expect(component).toBeTruthy()
  })
})
