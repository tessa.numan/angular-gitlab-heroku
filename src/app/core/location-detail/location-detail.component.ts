import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { Component, OnInit, Input } from '@angular/core'
import { filter, map, tap, switchMap } from 'rxjs/operators'

import { LocationService } from '../../../services/location.service'
import { LocationModel } from '../../../models/location.model'
import { AuthService } from 'src/services/auth.service'

@Component({
  selector: 'app-location-detail',
  templateUrl: './location-detail.component.html',
  styleUrls: ['./location-detail.component.scss']
})
export class LocationDetailComponent implements OnInit {
  @Input() locationModel: LocationModel

  constructor(
    private route: ActivatedRoute,
    private locationService: LocationService,
    private location: Location,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.getLocation()
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn()
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      console.log('log in')
      return false
    }
    return true
  }

  getLocation(): void {
    console.log('get location wordt aangeroepen')
    const id = this.route.snapshot.paramMap.get('id')
    const userid = this.route.snapshot.paramMap.get('userid')
    this.locationService.getLocation(userid, id).subscribe(location => (this.locationModel = location[0]))
  }

  update(): void {
    this.locationService.updateLocation(this.locationModel).subscribe(() => this.goBack())
  }

  goBack(): void {
    this.location.back()
  }
}
