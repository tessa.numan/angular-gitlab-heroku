import { Component, OnInit } from '@angular/core'
import { Musical } from '../../../models/musical.model'
import { MusicalService } from '../../../services/musical.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  musicals: Musical[] = []

  getMusicals(): void {
    this.musicalService
      .getMusicals()
      .subscribe(
        musicals => (this.musicals = musicals.sort((a, b) => a.name.localeCompare(b.name)).slice(0, 4))
      )
  }

  constructor(private musicalService: MusicalService) {}

  ngOnInit() {
    this.getMusicals()
  }
}
