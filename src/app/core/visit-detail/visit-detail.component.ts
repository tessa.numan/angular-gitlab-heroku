import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { Component, OnInit, Input } from '@angular/core'
import { filter, map, tap, switchMap } from 'rxjs/operators'

import { VisitService } from '../../../services/visit.service'
import { Visit } from '../../../models/visit.model'
import { AuthService } from 'src/services/auth.service'

@Component({
  selector: 'app-visit-detail',
  templateUrl: './visit-detail.component.html',
  styleUrls: ['./visit-detail.component.scss']
})
export class VisitDetailComponent implements OnInit {
  @Input() visit: Visit

  constructor(
    private route: ActivatedRoute,
    private visitService: VisitService,
    private location: Location,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.getVisit()
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn()
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      console.log('log in')
      return false
    }
    return true
  }

  getVisit(): void {
    console.log('get visit wordt aangeroepen')
    const id = this.route.snapshot.paramMap.get('id')
    const userid = this.route.snapshot.paramMap.get('userid')
    this.visitService.getVisit(userid, id).subscribe(visit => (this.visit = visit[0]))
  }

  update(): void {
    this.visitService.updateVisit(this.visit).subscribe(() => this.goBack())
  }

  goBack(): void {
    this.location.back()
  }
}
