import { TestBed, async, ComponentFixture } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { AppComponent } from './app.component'
import { UsecasesComponent } from '../../about/usecases/usecases.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { UsecaseComponent } from 'src/app/about/usecases/usecase/usecase.component'
import { DashboardComponent } from '../dashboard/dashboard.component'
import { NavbarComponent } from '../navbar/navbar.component'
import { Component, Directive, Input, HostListener } from '@angular/core'

const template = ''

@Component({ selector: 'app-alert', template })
class AlertStubComponent {}

@Component({ selector: 'router-outlet', template })
class RouterOutletStubComponent {}

@Component({ selector: 'app-messages', template })
class MessagesStubComponent {}

@Component({ selector: 'app-navbar', template })
class NavbarStubComponent {
  @Input() apptitle: string
}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, NgbModule],
      declarations: [
        AppComponent,
        AlertStubComponent,
        RouterOutletStubComponent,
        MessagesStubComponent,
        NavbarStubComponent
      ]
    }).compileComponents()
  }))

  fit('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app).toBeTruthy()
  })
})
