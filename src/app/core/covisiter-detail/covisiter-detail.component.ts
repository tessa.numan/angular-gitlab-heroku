import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { Component, OnInit, Input } from '@angular/core'
import { filter, map, tap, switchMap } from 'rxjs/operators'

import { CovisiterService } from '../../../services/covisiter.service'
import { Covisiter } from '../../../models/covisiter.model'
import { AuthService } from 'src/services/auth.service'

@Component({
  selector: 'app-covisiter-detail',
  templateUrl: './covisiter-detail.component.html',
  styleUrls: ['./covisiter-detail.component.scss']
})
export class CovisiterDetailComponent implements OnInit {
  @Input() covisiter: Covisiter

  constructor(
    private route: ActivatedRoute,
    private covisiterService: CovisiterService,
    private location: Location,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.getCovisiter()
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn()
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      console.log('log in')
      return false
    }
    return true
  }

  getCovisiter(): void {
    console.log('get covisiter wordt aangeroepen')
    const id = this.route.snapshot.paramMap.get('id')
    const userid = this.route.snapshot.paramMap.get('userid')
    this.covisiterService.getCovisiter(userid, id).subscribe(covisiter => (this.covisiter = covisiter[0]))
  }

  update(): void {
    this.covisiterService.updateCovisiter(this.covisiter).subscribe(() => this.goBack())
  }

  goBack(): void {
    this.location.back()
  }
}
