import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CovisiterDetailComponent } from './covisiter-detail.component'

describe('CovisiterDetailComponent', () => {
  let component: CovisiterDetailComponent
  let fixture: ComponentFixture<CovisiterDetailComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CovisiterDetailComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CovisiterDetailComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  xit('should create', () => {
    expect(component).toBeTruthy()
  })
})
