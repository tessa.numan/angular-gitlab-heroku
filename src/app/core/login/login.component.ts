import { Component, OnInit, Input } from '@angular/core'
import { User } from 'src/models/user.model'

import { ActivatedRoute, Router } from '@angular/router'
import { AuthService } from 'src/services/auth.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: User

  constructor(public authService: AuthService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.user = new User()
  }

  login(): void {
    console.log(this.user)
    console.log(this.authService)
    this.authService.login(this.user)
    this.router.navigate(['/dashboard'])
  }

  logout(): void {
    const loggedIn = this.authService.isLoggedIn()
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return
    }
    this.authService.logout()
    this.router.navigate(['/dashboard'])
  }
}
