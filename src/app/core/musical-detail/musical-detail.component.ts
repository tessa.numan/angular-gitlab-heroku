import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { Component, OnInit, Input } from '@angular/core'
import { filter, map, tap, switchMap } from 'rxjs/operators'

import { MusicalService } from '../../../services/musical.service'
import { Musical } from '../../../models/musical.model'
import { AuthService } from 'src/services/auth.service'

@Component({
  selector: 'app-musical-detail',
  templateUrl: './musical-detail.component.html',
  styleUrls: ['./musical-detail.component.scss']
})
export class MusicalDetailComponent implements OnInit {
  @Input() musical: Musical

  constructor(
    private route: ActivatedRoute,
    private musicalService: MusicalService,
    private location: Location,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.getMusical()
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn()
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      console.log('log in')
      return false
    }
    return true
  }

  getMusical(): void {
    console.log('get musical wordt aangeroepen')
    const id = this.route.snapshot.paramMap.get('id')
    const userid = this.route.snapshot.paramMap.get('userid')
    this.musicalService.getMusical(userid, id).subscribe(musical => (this.musical = musical[0]))
  }

  update(): void {
    this.musicalService.updateMusical(this.musical).subscribe(() => this.goBack())
  }

  goBack(): void {
    this.location.back()
  }
}
