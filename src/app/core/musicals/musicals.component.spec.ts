import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { MusicalsComponent } from './musicals.component'
import { FormsModule } from '@angular/forms'

describe('MusicalsComponent', () => {
  let component: MusicalsComponent
  let fixture: ComponentFixture<MusicalsComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [MusicalsComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicalsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  xit('should create', () => {
    expect(component).toBeTruthy()
  })
})
