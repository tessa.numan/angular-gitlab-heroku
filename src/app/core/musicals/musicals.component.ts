import { Component, OnInit } from '@angular/core'
import { Musical } from '../../../models/musical.model'
import { MusicalService } from '../../../services/musical.service'
import { MessageService } from '../../../services/message.service'
import { AuthService } from 'src/services/auth.service'

@Component({
  selector: 'app-musicals',
  templateUrl: './musicals.component.html',
  styleUrls: ['./musicals.component.scss']
})
export class MusicalsComponent implements OnInit {
  musicals: Musical[]
  addBool: boolean
  musical: Musical

  constructor(
    private musicalService: MusicalService,
    private authService: AuthService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.musicals = new Array()
    this.getMusicals()
    this.musical = new Musical()
  }

  getMusicals(): void {
    this.musicalService.getMusicals().subscribe(musicals => {
      this.musicals = musicals
      console.log(musicals)
    })
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn()
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      console.log('log in')
      return false
    }
    return true
  }

  addButton() {
    return (this.addBool = true)
  }

  add(): void {
    if (!this.loginCheck()) {
      return
    }
    if (!this.musical.name) {
      return
    }
    this.musical.name = this.musical.name.trim()
    if (this.musicals !== null) {
      const found = this.musicals.find(element => {
        return element.name === this.musical.name
      })
      console.log(found)
      if (found !== null && found !== undefined) {
        this.messageService.add(`MusicalComponent: name already in db`)
        return
      }
    }
    this.musicalService.addMusical(this.musical).subscribe(musical => {
      this.musicals.push(musical)
    })
  }

  delete(musical: Musical): void {
    if (!this.loginCheck()) {
      return
    }
    this.musicals = this.musicals.filter(m => m !== musical)
    this.musicalService.deleteMusical(musical).subscribe()
    console.log('Log in!')
  }
}
