import { Component, OnInit } from '@angular/core'
import { Visit } from '../../../models/visit.model'
import { VisitService } from '../../../services/visit.service'
import { MessageService } from '../../../services/message.service'
import { AuthService } from 'src/services/auth.service'
import { Musical } from 'src/models/musical.model'
import { MusicalService } from 'src/services/musical.service'
import { LocationService } from 'src/services/location.service'
import { CovisiterService } from 'src/services/covisiter.service'
import { LocationModel } from '../../../models/location.model'
import { Covisiter } from 'src/models/covisiter.model'

@Component({
  selector: 'app-visits',
  templateUrl: './visits.component.html',
  styleUrls: ['./visits.component.scss']
})
export class VisitsComponent implements OnInit {
  visit: Visit
  visits: Visit[]
  musical: Musical
  musicals: Musical[]
  locationModel: LocationModel
  locations: LocationModel[]
  addBool: boolean
  covisiters: Covisiter[]
  covisiter: Covisiter
  currentCovisiters: Covisiter[]

  constructor(
    private visitService: VisitService,
    private musicalService: MusicalService,
    private authService: AuthService,
    private messageService: MessageService,
    private locationService: LocationService,
    private covisiterService: CovisiterService
  ) {}

  ngOnInit() {
    this.visit = new Visit()
    this.visits = new Array()
    this.musical = new Musical()
    this.musicals = new Array()
    this.locationModel = new LocationModel()
    this.locations = new Array()
    this.covisiter = new Covisiter()
    this.covisiters = new Array()
    this.currentCovisiters = new Array()
    this.getVisits()
    this.getMusicals()
    this.getLocations()
    this.getCovisiters()
    this.visit.dvd = false
    this.visit.iswatched = true
  }

  getVisits(): void {
    if (!this.loginCheck()) {
      return
    }
    this.visitService.getVisits().subscribe(visits => {
      this.visits = visits
      console.log(visits)
    })
  }

  getMusicals(): void {
    this.musicalService.getMusicals().subscribe(musicals => {
      this.musicals = musicals
      console.log(musicals)
    })
  }

  getLocations(): void {
    this.locationService.getLocations().subscribe(locations => {
      this.locations = locations
      console.log(locations)
    })
  }

  getCovisiters(): void {
    this.covisiterService.getCovisiters().subscribe(covisiters => {
      this.covisiters = covisiters
      console.log(covisiters)
    })
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn()
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      console.log('log in')
      return false
    }
    return true
  }

  addButton() {
    return (this.addBool = true)
  }

  add(co: any): void {
    console.log(co.model)
    if (!this.loginCheck()) {
      return
    }

    if (!this.visit.musical) {
      console.log('return')
      return
    }

    if (!this.visit.date) {
      console.log('return, missing date')
      return
    }

    if (co !== undefined) {
      this.currentCovisiters.push(co.model)
      this.visit.covisiter = this.currentCovisiters
      console.log(this.visit)
    }

    // if (this.visit.covisiter){
    //   console.log(this.visit.covisiter)
    //   this.visit.covisiter
    // }

    // this.visitService.addVisit(this.visit).subscribe(visit => {
    //   this.visits.push(visit)
    // })
  }

  delete(visit: Visit): void {
    if (!this.loginCheck()) {
      return
    }
    this.visits = this.visits.filter(m => m !== visit)
    this.visitService.deleteVisit(visit).subscribe()
    console.log('Log in!')
  }
}
