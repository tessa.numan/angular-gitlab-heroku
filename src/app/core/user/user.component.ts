import { Component, OnInit, Input } from '@angular/core'
import { User } from 'src/models/user.model'
import { Location } from '@angular/common'

import { ActivatedRoute, Router } from '@angular/router'
import { AuthService } from 'src/services/auth.service'
import { UserService } from 'src/services/user.service'
import { AlertService } from 'src/services/alert.service'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  user: User

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.getUser()
  }

  getUser(): void {
    const loggedIn = this.authService.isLoggedIn()
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return
    }
    this.userService.getUser().subscribe(user => {
      this.user = user
      this.user.password = ''
      return this.user
    })

    console.log(this.user)
  }

  update(): void {
    const loggedIn = this.authService.isLoggedIn()
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return
    }
    if (!this.user.password || this.user.password === '') {
      this.alertService.error('Geef het huidige wachtwoord op')
      return
    }
    if (!this.user.newPassword || this.user.newPassword === '' || this.user.newPassword === undefined) {
      this.alertService.error('Geef het nieuwe wachtwoord op')
      return
    }
    console.log(this.user)
    this.userService.updateUser(this.user).subscribe(() => this.goBack())
    this.alertService.success('User aangepast')
  }

  goBack(): void {
    this.location.back()
  }
}
