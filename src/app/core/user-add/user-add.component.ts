import { Component, OnInit } from '@angular/core'
import { User } from 'src/models/user.model'
import { Router } from '@angular/router'
import { UserService } from 'src/services/user.service'
import { Location } from '@angular/common'

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {
  user: User
  users: User[]

  constructor(private router: Router, private userService: UserService, private location: Location) {}

  ngOnInit() {
    this.user = new User()
  }

  add(): void {
    console.log(this.user.username)
    if (!this.user.username || !this.user.password) {
      return
    }
    this.user.username = this.user.username.trim()
    this.userService.addUser(this.user).subscribe(() => this.goBack())
  }

  goBack(): void {
    this.location.back()
  }
}
