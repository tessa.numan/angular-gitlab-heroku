import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { MusicalsComponent } from './core/musicals/musicals.component'
import { MusicalDetailComponent } from './core/musical-detail/musical-detail.component'
import { LoginComponent } from './core/login/login.component'
import { UserComponent } from './core/user/user.component'
import { LocationsComponent } from './core/locations/locations.component'
import { LocationDetailComponent } from './core/location-detail/location-detail.component'
import { CovisitersComponent } from './core/covisiters/covisiters.component'
import { CovisiterDetailComponent } from './core/covisiter-detail/covisiter-detail.component'
import { VisitsComponent } from './core/visits/visits.component'
import { VisitDetailComponent } from './core/visit-detail/visit-detail.component'
import { UserAddComponent } from './core/user-add/user-add.component'

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'about', component: UsecasesComponent },
  { path: 'login', component: LoginComponent },
  { path: 'user', component: UserComponent },
  { path: 'user/add', component: UserAddComponent },
  { path: 'musicals', component: MusicalsComponent },
  { path: 'musical/:userid/:id', component: MusicalDetailComponent },
  { path: 'locations', component: LocationsComponent },
  { path: 'location/:userid/:id', component: LocationDetailComponent },
  { path: 'covisiters', component: CovisitersComponent },
  { path: 'covisiter/:userid/:id', component: CovisiterDetailComponent },
  { path: 'visits', component: VisitsComponent },
  { path: 'visit/:userid/:id', component: VisitDetailComponent },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
