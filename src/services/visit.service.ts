import { Injectable } from '@angular/core'
import { Observable, of, BehaviorSubject, observable, Subject, throwError } from 'rxjs'
import { map, tap, retry, catchError } from 'rxjs/operators'
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'

import { Visit } from '../models/visit.model'
import { MessageService } from './message.service'
import { environment } from '../environments/environment'
import { AuthService } from './auth.service'

@Injectable({
  providedIn: 'root'
})
export class VisitService {
  getCurrentuser() {
    const currentuser = localStorage.getItem('currentuser')
    return currentuser
  }

  getHttpOptions() {
    const token = localStorage.getItem('token')
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: 'Bearer ' + token })
    }
    return httpOptions
  }

  constructor(private http: HttpClient, private messageService: MessageService) {}

  // Get visits from the server
  getVisits(): Observable<Visit[]> {
    const userid = this.getCurrentuser()
    const httpOptions = this.getHttpOptions()
    return this.http.get<Visit[]>(`${environment.apiUrl}/visit/${userid}`, httpOptions).pipe(
      tap(_ => console.log('Fetched visits')),
      catchError(this.handleError<Visit[]>('getVisits', []))
    )
  }

  // Get visit by ID
  getVisit(userid: string, id: string): Observable<Visit> {
    const httpOptions = this.getHttpOptions()
    return this.http.get<Visit>(`${environment.apiUrl}/visit/${userid}/${id}`, httpOptions).pipe(
      tap(_ => console.log(`fetched visit id=${id}`)),
      tap(console.log),
      catchError(this.handleError<Visit>(`getVisit id=${id}`))
    )
  }

  // Post visit
  addVisit(visit: Visit): Observable<Visit> {
    const userid = this.getCurrentuser()
    if (userid) {
      visit.userid = userid
    } else {
      console.log('User niet ingelogd')
      return
    }
    const httpOptions = this.getHttpOptions()
    return this.http.post<Visit>(`${environment.apiUrl}/visit`, visit, httpOptions).pipe(
      tap((newVisit: Visit) => console.log(`Added visit with id=${newVisit._id}`)),
      catchError(this.handleError<Visit>('addVisit'))
    )
  }

  // Put visit
  updateVisit(visit: Visit): Observable<any> {
    const userid = this.getCurrentuser()
    if (userid) {
      visit.userid = userid
    } else {
      console.log('User niet ingelogd')
      return
    }
    const id = visit._id
    const httpOptions = this.getHttpOptions()
    return this.http.put(`${environment.apiUrl}/visit/${id}`, visit, httpOptions).pipe(
      tap(_ => console.log(`Updated visit id=${visit._id}`)),
      catchError(this.handleError<any>('updateVisit'))
    )
  }

  // Delete visit
  deleteVisit(visit: Visit): Observable<any> {
    const userid = this.getCurrentuser()
    const id = visit._id
    const httpOptions = this.getHttpOptions()
    return this.http.delete(`${environment.apiUrl}/visit/${id}`, httpOptions).pipe(
      tap(_ => console.log(`Deleted visit id=${visit._id}`)),
      catchError(this.handleError<any>('deleteVisit'))
    )
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
