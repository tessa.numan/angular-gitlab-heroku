import { Injectable } from '@angular/core'
import { Observable, of, BehaviorSubject, observable, Subject, throwError } from 'rxjs'
import { map, tap, retry, catchError } from 'rxjs/operators'
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'

import { Musical } from '../models/musical.model'
import { MessageService } from './message.service'
import { environment } from '../environments/environment'
import { AuthService } from './auth.service'

@Injectable({
  providedIn: 'root'
})
export class MusicalService {
  getCurrentuser() {
    const currentuser = localStorage.getItem('currentuser')
    return currentuser
  }

  getHttpOptions() {
    const token = localStorage.getItem('token')
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: 'Bearer ' + token })
    }
    return httpOptions
  }

  constructor(private http: HttpClient, private messageService: MessageService) {}

  // Get musicals from the server
  getMusicals(): Observable<Musical[]> {
    return this.http.get<Musical[]>(`${environment.apiUrl}/musical`).pipe(
      tap(_ => console.log('Fetched musicals')),
      catchError(this.handleError<Musical[]>('getMusicals', []))
    )
  }

  // Get musical by ID
  getMusical(userid: string, id: string): Observable<Musical> {
    return this.http.get<Musical>(`${environment.apiUrl}/musical/${userid}/${id}`).pipe(
      tap(_ => console.log(`fetched musical id=${id}`)),
      tap(console.log),
      catchError(this.handleError<Musical>(`getMusical id=${id}`))
    )
  }

  // Get musical by name
  getMusicalByName(nameIn: string): Observable<Musical> {
    const musical = { name: nameIn }
    return this.http.post<Musical>(`${environment.apiUrl}/musical/name`, musical).pipe(
      tap(_ => console.log(`fetched musical name=${nameIn}`)),
      tap(console.log),
      catchError(this.handleError<Musical>(`getMusical id=${nameIn}`))
    )
  }

  // Post musical
  addMusical(musical: Musical): Observable<Musical> {
    const userid = this.getCurrentuser()
    if (userid) {
      musical.userid = userid
    } else {
      console.log('User niet ingelogd')
      return
    }
    const httpOptions = this.getHttpOptions()
    return this.http.post<Musical>(`${environment.apiUrl}/musical`, musical, httpOptions).pipe(
      tap((newMusical: Musical) => console.log(`Added musical with id=${newMusical._id}`)),
      catchError(this.handleError<Musical>('addMusical'))
    )
  }

  // Put musical
  updateMusical(musical: Musical): Observable<any> {
    const userid = this.getCurrentuser()
    if (userid) {
      musical.userid = userid
    } else {
      console.log('User niet ingelogd')
      return
    }
    const id = musical._id
    const httpOptions = this.getHttpOptions()
    return this.http.put(`${environment.apiUrl}/musical/${id}`, musical, httpOptions).pipe(
      tap(_ => console.log(`Updated musical id=${musical._id}`)),
      catchError(this.handleError<any>('updateMusical'))
    )
  }

  // Delete musical
  deleteMusical(musical: Musical): Observable<any> {
    const userid = this.getCurrentuser()
    const id = musical._id
    const httpOptions = this.getHttpOptions()
    return this.http.delete(`${environment.apiUrl}/musical/${id}`, httpOptions).pipe(
      tap(_ => console.log(`Deleted musical id=${musical._id}`)),
      catchError(this.handleError<any>('deleteMusical'))
    )
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
