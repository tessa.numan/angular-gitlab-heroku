import { Injectable } from '@angular/core'
import { MessageService } from './message.service'
import { User } from 'src/models/user.model'
import { Observable, of, BehaviorSubject, observable, Subject, throwError } from 'rxjs'
import { map, tap, retry, catchError } from 'rxjs/operators'
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  getCurrentuser() {
    const currentuser = localStorage.getItem('currentuser')
    return currentuser
  }

  getHttpOptions() {
    const token = localStorage.getItem('token')
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: 'Bearer ' + token })
    }
    return httpOptions
  }

  constructor(private http: HttpClient, private messageService: MessageService) {}

  // Get user by ID
  getUser(): Observable<User> {
    let id
    const userid = this.getCurrentuser()
    if (userid) {
      id = userid
    } else {
      console.log('User niet ingelogd')
      return
    }
    const httpOptions = this.getHttpOptions()
    return this.http.get<User>(`${environment.apiUrl}/user/${id}`, httpOptions).pipe(
      tap(_ => console.log(`fetched user id=${id}`)),
      tap(console.log),
      catchError(this.handleError<User>(`getUser id=${id}`))
    )
  }

  // Post location
  addUser(user: User): Observable<User> {
    console.log('add')
    return this.http.post<User>(`${environment.apiUrl}/user`, user).pipe(
      tap(console.log),
      catchError(this.handleError<User>('addUser'))
    )
  }

  // Put musical
  updateUser(user: User): Observable<any> {
    let id
    const userid = this.getCurrentuser()
    if (userid) {
      id = userid
    } else {
      console.log('User niet ingelogd')
      return
    }
    const httpOptions = this.getHttpOptions()
    return this.http.put(`${environment.apiUrl}/user/${id}`, user, httpOptions).pipe(
      tap(_ => console.log(`Updated user id=${user._id}`)),
      catchError(this.handleError<any>('updateMusical'))
    )
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
