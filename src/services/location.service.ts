import { Injectable } from '@angular/core'
import { Observable, of, BehaviorSubject, observable, Subject, throwError } from 'rxjs'
import { map, tap, retry, catchError } from 'rxjs/operators'
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'

import { LocationModel } from '../models/location.model'
import { MessageService } from './message.service'
import { environment } from '../environments/environment'
import { AuthService } from './auth.service'

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  getCurrentuser() {
    const currentuser = localStorage.getItem('currentuser')
    return currentuser
  }

  getHttpOptions() {
    const token = localStorage.getItem('token')
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: 'Bearer ' + token })
    }
    return httpOptions
  }

  constructor(private http: HttpClient, private messageService: MessageService) {}

  // Get locations from the server
  getLocations(): Observable<LocationModel[]> {
    return this.http.get<LocationModel[]>(`${environment.apiUrl}/location`).pipe(
      tap(_ => console.log('Fetched locations')),
      catchError(this.handleError<LocationModel[]>('getLocations', []))
    )
  }

  // Get location by ID
  getLocation(userid: string, id: string): Observable<Location> {
    return this.http.get<LocationModel>(`${environment.apiUrl}/location/${userid}/${id}`).pipe(
      tap(_ => console.log(`fetched location id=${id}`)),
      tap(console.log),
      catchError(this.handleError<LocationModel>(`getLocation id=${id}`))
    )
  }

  // Post location
  addLocation(location: LocationModel): Observable<LocationModel> {
    const userid = this.getCurrentuser()
    if (userid) {
      location.userid = userid
    } else {
      console.log('User niet ingelogd')
      return
    }
    const httpOptions = this.getHttpOptions()
    return this.http.post<LocationModel>(`${environment.apiUrl}/location`, location, httpOptions).pipe(
      tap((newLocation: LocationModel) => console.log(`Added location with id=${newLocation._id}`)),
      catchError(this.handleError<LocationModel>('addLocation'))
    )
  }

  // Put location
  updateLocation(location: LocationModel): Observable<any> {
    const userid = this.getCurrentuser()
    if (userid) {
      location.userid = userid
    } else {
      console.log('User niet ingelogd')
      return
    }
    const id = location._id
    const httpOptions = this.getHttpOptions()
    return this.http.put(`${environment.apiUrl}/location/${id}`, location, httpOptions).pipe(
      tap(_ => console.log(`Updated location id=${location._id}`)),
      catchError(this.handleError<any>('updateLocation'))
    )
  }

  // Delete location
  deleteLocation(location: LocationModel): Observable<any> {
    const userid = this.getCurrentuser()
    const id = location._id
    const httpOptions = this.getHttpOptions()
    return this.http.delete(`${environment.apiUrl}/location/${id}`, httpOptions).pipe(
      tap(_ => console.log(`Deleted location id=${location._id}`)),
      catchError(this.handleError<any>('deleteLocation'))
    )
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
