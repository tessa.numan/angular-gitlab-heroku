import { Injectable } from '@angular/core'
import { Observable, of, BehaviorSubject, observable, Subject, throwError } from 'rxjs'
import { map, tap, retry, catchError } from 'rxjs/operators'
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'

import { Covisiter } from '../models/covisiter.model'
import { MessageService } from './message.service'
import { environment } from '../environments/environment'
import { AuthService } from './auth.service'

@Injectable({
  providedIn: 'root'
})
export class CovisiterService {
  getCurrentuser() {
    const currentuser = localStorage.getItem('currentuser')
    return currentuser
  }

  getHttpOptions() {
    const token = localStorage.getItem('token')
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: 'Bearer ' + token })
    }
    return httpOptions
  }

  constructor(private http: HttpClient, private messageService: MessageService) {}

  // Get covisiters from the server
  getCovisiters(): Observable<Covisiter[]> {
    const userid = this.getCurrentuser()
    const httpOptions = this.getHttpOptions()
    return this.http.get<Covisiter[]>(`${environment.apiUrl}/covisitor/${userid}`, httpOptions).pipe(
      tap(_ => console.log('Fetched covisiters')),
      catchError(this.handleError<Covisiter[]>('getCovisiters', []))
    )
  }

  // Get covisiter by ID
  getCovisiter(userid: string, id: string): Observable<Covisiter> {
    const httpOptions = this.getHttpOptions()
    return this.http.get<Covisiter>(`${environment.apiUrl}/covisitor/${userid}/${id}`, httpOptions).pipe(
      tap(_ => console.log(`fetched covisiter id=${id}`)),
      tap(console.log),
      catchError(this.handleError<Covisiter>(`getCovisiter id=${id}`))
    )
  }

  // Post covisiter
  addCovisiter(covisiter: Covisiter): Observable<Covisiter> {
    const userid = this.getCurrentuser()
    if (userid) {
      covisiter.userid = userid
    } else {
      console.log('User niet ingelogd')
      return
    }
    const httpOptions = this.getHttpOptions()
    return this.http.post<Covisiter>(`${environment.apiUrl}/covisitor`, covisiter, httpOptions).pipe(
      tap((newCovisiter: Covisiter) => console.log(`Added covisiter with id=${newCovisiter._id}`)),
      catchError(this.handleError<Covisiter>('addCovisiter'))
    )
  }

  // Put covisiter
  updateCovisiter(covisiter: Covisiter): Observable<any> {
    const userid = this.getCurrentuser()
    if (userid) {
      covisiter.userid = userid
    } else {
      console.log('User niet ingelogd')
      return
    }
    const id = covisiter._id
    const httpOptions = this.getHttpOptions()
    return this.http.put(`${environment.apiUrl}/covisitor/${id}`, covisiter, httpOptions).pipe(
      tap(_ => console.log(`Updated covisiter id=${covisiter._id}`)),
      catchError(this.handleError<any>('updateCovisiter'))
    )
  }

  // Delete covisiter
  deleteCovisiter(covisiter: Covisiter): Observable<any> {
    const userid = this.getCurrentuser()
    const id = covisiter._id
    const httpOptions = this.getHttpOptions()
    return this.http.delete(`${environment.apiUrl}/covisitor/${id}`, httpOptions).pipe(
      tap(_ => console.log(`Deleted covisiter id=${covisiter._id}`)),
      catchError(this.handleError<any>('deleteCovisiter'))
    )
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
