import { Observable, from } from 'rxjs'
import { map, reduce } from 'rxjs/operators'
import { Pipe, PipeTransform } from '@angular/core'

export class LocationModel {
  _id: string
  name: string
  userid: string
  address: string
  phone: string

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
