import { Observable, from } from 'rxjs'
import { map, reduce } from 'rxjs/operators'
import { Pipe, PipeTransform } from '@angular/core'

export class Musical {
  _id: string
  userid: string
  name: string
  note: string

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
