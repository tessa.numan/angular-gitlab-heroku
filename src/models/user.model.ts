import { Observable, from } from 'rxjs'
import { map, reduce } from 'rxjs/operators'
import { Pipe, PipeTransform } from '@angular/core'

export class User {
  _id: string
  username: string
  password: string
  newPassword: string

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
