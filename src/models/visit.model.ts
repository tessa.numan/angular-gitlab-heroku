import { Observable, from } from 'rxjs'
import { map, reduce } from 'rxjs/operators'
import { Pipe, PipeTransform } from '@angular/core'
import { Musical } from './musical.model'
import { Covisiter } from './covisiter.model'

export class Visit {
  _id: string
  musical: Musical
  userid: string
  date: Date
  rating: number
  chair: string
  note: string
  location: string
  covisiter: Covisiter[]
  iswatched: boolean
  dvd: boolean

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
